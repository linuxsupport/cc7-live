all: iso

iso:
	LANG=C livecd-creator --config=cc-7x-livecd.ks --fslabel="CC7-LiveCD" --tmpdir=/tmp/cc7-live-tmp/ --cache=/tmp/cc7-live-cache/ --debug
