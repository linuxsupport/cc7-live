# CC7 Live Image

This a recipe to create a live imaage that can be booted through aims2.

Please note that we do not support a centrally supported live image, you can adapt it and upload your own to aims2.

Customization example:
- https://gitlab.cern.ch/hroussea/st-live-image/ (it can run a script passed on the cmdline)
- https://gitlab.cern.ch/lhcb-test-images/lhcb-live

## How to build

### Pre-flight checks

 * You need a fully updated CERN CentOS 7 node
 * You need to disable selinux during the build `setenforce 0`
 * `yum install livecd-tools`
 * Modify the `cc-7x-livecd.ks` so it fits your needs.

### Build the ISO

 * `make`
 * It will generate the `CC7-LiveCD.iso` ISO.

### Prepare for aims2

 * `livecd-iso-to-pxeboot ./CC7-LiveCD.iso`
 * It will create a pxeboot/ folder ; move it a public directory on afs and run :
 `aims2 addimg --arch x86_64 --desc 'My LiveCD CC7' --vmlinuz ./vmlinuz0 --initrd ./initrd0.img --name MY_CC_7_LIVE --kopts 'rootflags=loop root=live:/CC7-LiveCD.iso rootfstype=auto ro rd.live.image'`
 * Please note that the `root=live:/CC7-LiveCD.iso` must match the option you passed to livecd-tools

### Register a hostname

 * `aims2 addhost <hostname> --kopts="rootflags=loop root=live:/CC7-LiveCD.iso rootfstype=auto ro liveimg" MY_CC_7_LIVE`
 * `aims2 pxeon <hostname> MY_CC_7_LIVE`

### Install on a USB stick

 * `livecd-iso-to-disk CC7-LiveCD.iso /dev/sdXX`
